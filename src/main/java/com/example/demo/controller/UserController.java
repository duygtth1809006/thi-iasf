package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    UserRepository userRepository;
    @@RequestMapping("/")
    Public String index(Model model){
        List<User> users = (List<User>) userRepository.findAll();
        model.addAttribute(s:"users",users);
        return"index";
    }
    @@RequestMapping(value="add")
    public String addUser(Model model){
        model.addAllAttributes(s:"user",new User());
        return "addUser";
    }
    @@RequestMapping(Value = "/seve",method = RequestMethod.POST)
        public String seve(User user){
        userRepository.seve(user);
        return "redirect";
    }
    @RequestMapping uesMapping(value = "/edit",method = RequestMethod.GET)
    public String editUser(@RequestParam uestParam("id")Long userId, Model model){
        Optional<User> userEdit = userRepository.findById(userId);
        userEdit.ifPresent(user -> model.addAttribute(s:"user",user));
        return "editUser";
    }
    @RequestMapping uesMapping(value = "delete", method = RequestMethod.DELETE)
    public String deleteUser(@RequestParam("id")Long userId, Model model){
        userRepository.deleteById(userId);
        return "redirect:/";
    }


}
